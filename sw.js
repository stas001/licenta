const staticCacheName = 'site-static-v1';
const dinamicCacheName = 'site-dinamic-v1';

const assets = [
    '/',
    '/index.html',
    '/js/ui.js',
    'js/app.js',
    '/js/materialize.min.js',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    '/css/materialize.min.css',
    '/css/styles.css',
    '/img/icon_256_256.png',
    'pages/fallback.html'
];

//install service worker
self.addEventListener('install', e =>{
    //console.log('service worker has been installed');
    e.waitUntil(
        caches.open(staticCacheName).then(cache => {
            console.log("caching shell assets");
            cache.addAll(assets);
        })
    );
}); 
//

//activate service worker(event activate)
self.addEventListener('activate', e =>{
    //console.log('service worker has been activated');

    e.waitUntil(
        caches.keys().then(keys =>{
            // console.log(keys);
            return Promise.all(keys
                .filter(key => key !== staticCacheName)
                .map(key => caches.delete(key))//
            )
        })
    )
});

//fetch event

self.addEventListener('fetch', e => {
  //console.log('fetch event', evt);
  e.respondWith(
    caches.match(evt.request).then(cacheRes => {
      return cacheRes || fetch(evt.request).then(fetchRes => {
        return caches.open(dynamicCacheName).then(cache => {
          cache.put(evt.request.url, fetchRes.clone());
          return fetchRes;
        })
      });
    }).catch(() => caches.match('/pages/fallback.html'))
  );
});