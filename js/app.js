//register serviceWorker
if('serviceWorker' in navigator){
    navigator.serviceWorker.register('/sw.js')
        .then((reg) => {
            console.log('Service Worker registred', reg)
        })
        .catch((err) => {
            console.log('Service Worker not registred', err)
        })
}